import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routes } from './app.routes';


import { AppComponent } from './app.component';
import { YalaComponent } from './yala/yala.component';
import { PackagesComponent } from './packages/packages.component';


@NgModule({
  declarations: [
    AppComponent,
    YalaComponent,
    PackagesComponent
  ],
  imports: [
    BrowserModule,
    routes,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
