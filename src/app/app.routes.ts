import { ModuleWithProviders} from '@angular/core';
import { Routes ,RouterModule} from '@angular/router'

import { AppComponent } from './app.component';
import { YalaComponent } from './yala/yala.component';
import { PackagesComponent } from './packages/packages.component';

export  const router: Routes =[
    {path: '' , redirectTo:'packages' , pathMatch:'full'},
    {path: 'yala' , component:YalaComponent},
    {path: 'packages' , component:PackagesComponent}

];

export const routes: ModuleWithProviders = RouterModule.forRoot(router);